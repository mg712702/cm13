#!/system/bin/sh
if [ -f /system/etc/recovery-transform.sh ]; then
  exec sh /system/etc/recovery-transform.sh 13801472 115edb04213e8106610596c9ba2cb3e25b0b0caa 10971136 31db1ebc0659fcd3ea3119164248124244819e7d
fi

if ! applypatch -c EMMC:/dev/block/bootdevice/by-name/recovery:13801472:115edb04213e8106610596c9ba2cb3e25b0b0caa; then
  applypatch -b /system/etc/recovery-resource.dat EMMC:/dev/block/bootdevice/by-name/boot:10971136:31db1ebc0659fcd3ea3119164248124244819e7d EMMC:/dev/block/bootdevice/by-name/recovery 115edb04213e8106610596c9ba2cb3e25b0b0caa 13801472 31db1ebc0659fcd3ea3119164248124244819e7d:/system/recovery-from-boot.p && log -t recovery "Installing new recovery image: succeeded" || log -t recovery "Installing new recovery image: failed"
else
  log -t recovery "Recovery image already installed"
fi
